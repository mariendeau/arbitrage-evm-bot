// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/token/ERC20/ERC20.sol";

contract Token is ERC20 {
    constructor() ERC20("Fixed", "FIX") {
        _mint(msg.sender, 10000000 ether);
    }
}