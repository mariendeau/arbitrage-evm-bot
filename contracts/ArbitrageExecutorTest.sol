// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/utils/Strings.sol";
import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/access/Ownable.sol";

import "./interfaces/IERC3156FlashBorrower.sol";
import "./interfaces/IERC3156FlashLender.sol";
import "./interfaces/IToken.sol";
import "./interfaces/UniswapRouter.sol";

contract ArbitrageExecutorTest is Ownable, IERC3156FlashBorrower{

    struct ArbitrageSpec{
        address lender;
        uint tradingAmount;
        address sender;
    }

    constructor () {
    }

    function flashLoan(
        address lender_,
        address token,
        uint amount
    ) public {
        IERC3156FlashLender lender = IERC3156FlashLender(lender_);

        bytes memory data = abi.encode(ArbitrageSpec(lender_, amount, msg.sender));

        uint256 _allowance = IToken(token).allowance(address(this), address(lender));
        uint256 _fee = lender.flashFee(token, amount);
        uint256 _repayment = amount + _fee;
        IToken(token).approve(address(lender), _allowance + _repayment);
        lender.flashLoan(this, token, amount, data);
    }

    function onFlashLoan(
        address initiator,
        address token,
        uint256 amount,
        uint256 fee,
        bytes calldata data
    ) external override returns(bytes32) {
        (ArbitrageSpec memory spec) = abi.decode(data, (ArbitrageSpec));
        
        require(
            msg.sender == address(spec.lender),
            "FlashBorrower: Untrusted lender"
        );
        require(
            initiator == address(this),
            "FlashBorrower: Untrusted loan initiator"
        );

        // require(false, Strings.toString(IToken(token).balanceOf(address(this))));

        return keccak256("ERC3156FlashBorrowerInterface.onFlashLoan");
    }

}

