// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/utils/Strings.sol";
import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/access/Ownable.sol";

import "./interfaces/IERC3156FlashBorrower.sol";
import "./interfaces/IERC3156FlashLender.sol";
import "./interfaces/IToken.sol";
import "./interfaces/UniswapRouter.sol";

contract ArbitrageExecutor is Ownable, IERC3156FlashBorrower{

    struct ArbitrageSpec{
        address lender;
        address routerFrom;
        address routerTo;
        address[] pathFrom;
        address[] pathTo;
        uint tradingAmount;
        address sender;
        string successMessage;
    }

    constructor () {
    }

    function executeArbitrage(
        address lender_,
        address routerFrom,
        address routerTo,
        address[] memory pathFrom,
        address[] memory pathTo,
        uint tradingAmount,
        string memory successMessage
    ) public {
        IERC3156FlashLender lender = IERC3156FlashLender(lender_);

        bytes memory data = abi.encode(ArbitrageSpec(lender_, routerFrom, routerTo, pathFrom, pathTo, tradingAmount, msg.sender, successMessage));

        uint256 _allowance = IToken(pathFrom[0]).allowance(address(this), address(lender));
        uint256 _fee = lender.flashFee(pathFrom[0], tradingAmount);
        uint256 _repayment = tradingAmount + _fee;
        IToken(pathFrom[0]).approve(address(lender), _allowance + _repayment);
        lender.flashLoan(this, pathFrom[0], tradingAmount, data);
    }

    // Hardcoded slippage of 0.5%
    function applySlippage(uint amount) internal view returns (uint){
        return amount * 9950 / 10000;
    }


    // require(false, Strings.toString(amountSwapped));
    function onFlashLoan(
        address initiator,
        address token,
        uint256 amount,
        uint256 fee,
        bytes calldata data
    ) external override returns(bytes32) {
        (ArbitrageSpec memory spec) = abi.decode(data, (ArbitrageSpec));
        
        require(
            msg.sender == address(spec.lender),
            "FlashBorrower: Untrusted lender"
        );
        require(
            initiator == address(this),
            "FlashBorrower: Untrusted loan initiator"
        );

        uint[] memory amountsResults;

        //ROUTER 1 SWAP 
        uint[] memory amountsOut = UniswapRouter(spec.routerFrom).getAmountsOut(amount, spec.pathFrom);
        
        IToken(token).approve(spec.routerFrom, amount);
        amountsResults = UniswapRouter(spec.routerFrom).swapExactTokensForTokens(amount, applySlippage(amountsOut[amountsOut.length-1]), spec.pathFrom, address(this), block.timestamp + 20);
        
        uint amountSwapped = amountsResults[amountsResults.length - 1];

        //ROUTER 2 SWAP 
        amountsOut = UniswapRouter(spec.routerTo).getAmountsOut(amountSwapped, spec.pathTo);
        
        IToken(spec.pathTo[0]).approve(spec.routerTo, amountSwapped);
        amountsResults = UniswapRouter(spec.routerTo).swapExactTokensForTokens(amountSwapped, applySlippage(amountsOut[amountsOut.length-1]), spec.pathTo, address(this), block.timestamp + 20);

        amountSwapped = amountsResults[amountsResults.length - 1];
        require(amountSwapped > amount + fee, "Arbitrage invalid");

        //Profits back to caller
        IToken(token).transfer(spec.sender, amountSwapped - (amount + fee));

        return keccak256(abi.encodePacked(spec.successMessage));
    }

}

