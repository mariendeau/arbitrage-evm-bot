// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/access/Ownable.sol";
import "./interfaces/IToken.sol";
import "./interfaces/UniswapRouter.sol";

contract ArbitrageFinder is Ownable{
    
    constructor() {
    }


    function getMultipleUniswapExchangeStats(uint multiplier, address[] memory routerAddresses, uint8[] memory routingPathLength, address[] memory routingPath) public view returns(uint[] memory bids, uint[] memory asks, uint[] memory spreads, uint[] memory spreadPcts, uint8[] memory decimalss){
        bids = new uint[](routingPathLength.length);
        asks = new uint[](routingPathLength.length);
        spreads = new uint[](routingPathLength.length);
        spreadPcts = new uint[](routingPathLength.length);
        decimalss = new uint8[](routingPathLength.length);

        uint8 index;
        for(uint i; i < routingPathLength.length; i++){
            address[] memory currentRoutingPath = new address[](routingPathLength[i]);
        
            for(uint j; j < routingPathLength[i]; j++){
                currentRoutingPath[j] = routingPath[index++];
            }
            
            (bids[i], asks[i], spreads[i], spreadPcts[i], decimalss[i]) = getUniswapExchangeStats(multiplier, routerAddresses[i], currentRoutingPath);
        }
    }

    function getUniswapExchangeStats(uint baseInput, address routerAddress, address[] memory routingPath) public view returns(uint bid, uint ask, uint spread, uint spreadPct, uint8 decimals){
        require(routingPath.length > 1, "RoutingPath must have more than 1 path entries");

        UniswapRouter router = UniswapRouter(routerAddress);
        uint[] memory amountsOut = router.getAmountsOut(baseInput, routingPath);

        bid = amountsOut[routingPath.length - 1];

        address[] memory reverseRoutingPath = new address[](routingPath.length);

        IToken token = IToken(routingPath[routingPath.length - 1]);
        decimals = token.decimals();

        uint index = routingPath.length;
        for (uint i; i < routingPath.length; i++) {
            reverseRoutingPath[i] = routingPath[--index];
        }

        uint[] memory amountsIn = router.getAmountsIn(baseInput, reverseRoutingPath);

        ask = amountsIn[0];

        spread = ask - bid;
        spreadPct = spread * 10**decimals / ask;
    }
}

