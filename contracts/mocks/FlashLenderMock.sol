// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import '../interfaces/IERC3156FlashLender.sol';
import '../interfaces/IToken.sol';

contract FlashLenderMock is IERC3156FlashLender{

    function maxFlashLoan(
        address token
    ) external pure returns (uint256){
        return type(uint256).max;
    }

    function flashFee(
        address token,
        uint256 amount
    ) external pure returns (uint256){
        //0.1% fee
        return amount / 1000;
    }

    function flashLoan(
        IERC3156FlashBorrower receiver,
        address token,
        uint256 amount,
        bytes calldata data
    ) external returns (bool) {

        IToken(token).transfer(address(receiver), amount);

        receiver.onFlashLoan(msg.sender, token, amount, amount / 1000, data);

        require(
            IToken(token).balanceOf(address(receiver)) >= amount + amount / 1000,
            "Flash Loan and Fees cannot be returns to lender"
        );

        IToken(token).transferFrom(address(receiver), address(this), amount + amount / 1000);

        return true;
    }
}