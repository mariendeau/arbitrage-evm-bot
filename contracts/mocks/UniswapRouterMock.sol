
// SPDX-License-Identifier: MIT

pragma solidity ^0.8.15;

import '../interfaces/IToken.sol';
import "OpenZeppelin/openzeppelin-contracts@4.7.0/contracts/utils/Strings.sol";

contract UniswapRouterMock{
    
    uint ask_;
    uint bid_;
    uint8 decimals_;

    constructor(uint ask, uint bid, uint8 decimals) {
        ask_ = ask;
        bid_ = bid;
        decimals_ = decimals;
    }

    function getAmountsOut(uint amountIn, address[] memory path)
        public
        view
        virtual
        returns (uint[] memory amounts)
    {
        amounts = new uint[](path.length);
        for (uint i; i < path.length; i++) {
            if(i<path.length - 1){
                amounts[i] = 0;
            }else{
                amounts[i] = amountIn * bid_ / 10**decimals_;
            }
        }
    }

    function getAmountsIn(uint amountOut, address[] memory path)
        public
        view
        virtual
        returns (uint[] memory amounts)
    {
        amounts = new uint[](path.length);
        for (uint i; i < path.length; i++) {
            if(i == 0){
                amounts[i] = amountOut * ask_ / 10**decimals_;
            }else{
                amounts[i] = 0;
            }
        }
    }

    //Token -> Token
    function swapExactTokensForTokens(uint256 amountIn, uint256 amountOutMin, address[] calldata path, address to, uint256 deadline) 
        external virtual returns (uint256[] memory amounts) {
        IToken(path[0]).transferFrom(to, address(this), amountIn);
        amounts = new uint256[](2);
        amounts[1] = amountOutMin;
        IToken(path[1]).transfer(to, amountOutMin);

    }

}

