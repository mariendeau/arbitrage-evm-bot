import brownie
from brownie import accounts, Wei, chain, ArbitrageFinder
import time

def test_deployContract(): 
   contract = ArbitrageFinder.deploy({'from': accounts[0]})
   assert contract
   time.sleep(1)
