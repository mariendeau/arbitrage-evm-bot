import brownie
from brownie import accounts, Wei, chain, ArbitrageExecutor, UniswapRouterMock, Token, FlashLenderMock
import time

def test_executeTokenToToken():
   lender = FlashLenderMock.deploy({'from': accounts[0]})

   # 1 / 1000 =  0.001 , 1/1005 = 0.001005
   routerFrom = UniswapRouterMock.deploy(Wei('0.001 ether'), Wei('0.001005 ether'), 18, {'from': accounts[0]})

   # 1 / 1010 =  0.00101 , 1/1015 = 0.001015
   routerTo = UniswapRouterMock.deploy(Wei('1010 ether'), Wei('1015 ether'), 18, {'from': accounts[0]})

   token = Token.deploy({'from': accounts[0]})
   wbnb = Token.deploy({'from': accounts[0]})

   token.transfer(lender.address, Wei('100000 ether'), {'from': accounts[0]})
   token.transfer(routerTo.address, Wei('100000 ether'), {'from': accounts[0]})
   wbnb.transfer(routerFrom.address, Wei('100000 ether'), {'from': accounts[0]})
   
   contract = ArbitrageExecutor.deploy({'from': accounts[0]})

   value = contract.executeArbitrage(lender.address, routerFrom, routerTo, [token, wbnb], [wbnb, token], Wei('10000 ether'), "anything" , {'from': accounts[1]}) 
   value.info()

   # Profit = 88.99 of token
   assert token.balanceOf(accounts[1]) == '88997518750000000000'

   time.sleep(1) 
