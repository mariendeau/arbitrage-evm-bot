import brownie
from brownie import accounts, Wei, chain, ArbitrageFinder, UniswapRouterMock, Token
import time

def test_getUniswapExchangeStats(): 
   router = UniswapRouterMock.deploy( Wei('337 ether'), Wei('335 ether'), 18, {'from': accounts[0]})
   token = Token.deploy({'from': accounts[0]})

   contract = ArbitrageFinder.deploy({'from': accounts[0]})

   # pcsRouter = '0x10ED43C718714eb63d5aA57B78B54704E256024E'
   path = ['0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', token.address]
   
   value = contract.getUniswapExchangeStats(Wei('1 ether'), router.address, path) 
   # assert value == 0
   assert value[0] == Wei('335 ether') #bid
   assert value[1] == Wei('337 ether') #ask
   assert value[2] == Wei(str(337 - 335) + ' ether') #spread
   assert value[3] == '5934718100890207' #spreadPct (337 - 335) / 337

   time.sleep(1)

def test_getMultipleUniswapExchangeStats(): 
   router = UniswapRouterMock.deploy( Wei('337 ether'), Wei('335 ether'), 18, {'from': accounts[0]})
   router2 = UniswapRouterMock.deploy( Wei('336.5 ether'), Wei('336 ether'), 18, {'from': accounts[0]})
   router3 = UniswapRouterMock.deploy( Wei('337 ether'), Wei('336.4 ether'), 18, {'from': accounts[0]})

   token = Token.deploy({'from': accounts[0]})

   contract = ArbitrageFinder.deploy({'from': accounts[0]})

   paths = ['0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', token.address, '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', token.address, '0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c', token.address]
   routers = [router.address, router2.address, router3.address]
   value = contract.getMultipleUniswapExchangeStats(Wei('1 ether'), routers, [2,2, 2], paths) 

   assert value[0][0] == Wei('335 ether') #bid
   assert value[1][0] == Wei('337 ether') #ask
   assert value[2][0] == Wei(str(337 - 335) + ' ether') #spread
   assert value[3][0] == '5934718100890207' #spreadPct (337 - 335) / 337

   assert value[0][1] == Wei('336 ether') #bid
   assert value[1][1] == Wei('336.5 ether') #ask
   assert value[2][1] == Wei(str(336.5 - 336) + ' ether') #spread
   assert value[3][1] == '1485884101040118' #spreadPct

   assert value[0][2] == Wei('336.4 ether') #bid
   assert value[1][2] == Wei('337 ether') #ask
   assert value[2][2] == Wei('0.6 ether') #spread
   assert value[3][2] == '1780415430267062' #spreadPct

   time.sleep(1)