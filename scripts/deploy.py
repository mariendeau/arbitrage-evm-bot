from brownie import accounts, ArbitrageFinder, ArbitrageExecutor, ArbitrageExecutorTest
import os

def main():
    account = accounts.add(os.getenv("PRIVATE_KEY"))
    # contract = ArbitrageFinder.deploy({'from': account, 'gas': 1000000})    
    contract = ArbitrageExecutor.deploy({'from': account, 'gas': 2000000}) 
    # contract = ArbitrageExecutorTest.deploy({'from': account, 'gas': 2000000}) 
    
    # print(contract1)
    print(contract)
    